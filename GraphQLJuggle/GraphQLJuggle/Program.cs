﻿using System;
using GraphQL;
using GraphQL.Types;


namespace GraphQLJuggle
{
    class Program
    {
        static void Main(string[] args)
        {
            var schema = Schema.For(@"
                type Person {
                    name: String,
                    age: Int,
                    id: ID
                }
  
                type Query { 
                    hello: String,
                    persons: [Person],
                    person(id: ID): Person
                }
            ", _ =>
            {
                _.Types.Include<Query>();
            });

            var json = schema.Execute(_ =>
            {
                _.Query = "{ person(id: 1) { name, age, id} }";
            });

            //var json = schema.Execute(_ =>
            //{
            //    _.Query = "{ persons { name, age } }";
            //});

            Console.WriteLine(json);
        }
    }
}
