﻿using System.Collections.Generic;

namespace GraphQLJuggle
{
    public static class PersonsDB
    {
        public static IEnumerable<Person> GetPersons() =>
            new List<Person>
            {
                new Person { Id = 1, Name = "Wilco", Age = 25 },
                new Person { Id = 2, Name = "Henk", Age = 52 }
            };
    }
}