﻿using System.Linq;
using GraphQL;
using System.Collections.Generic;

namespace GraphQLJuggle
{
    public class Query
    {
        [GraphQLMetadata("persons")]
        public IEnumerable<Person> GetPerons() => PersonsDB.GetPersons();

        [GraphQLMetadata("person")]
        public Person GetPerson(int id) =>
            PersonsDB.GetPersons().SingleOrDefault(x => x.Id == id);

        [GraphQLMetadata("hello")]
        public string GetHello() => "Hello Query Class";
    }
}